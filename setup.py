from setuptools import setup, find_packages


setup(
    name='common',
    author='Kristian Klemon',
    author_email='kristian.klemon@gmail.com',
    description='Common utilities for datascience and machine learning',
    url="https://github.com/kklemon/keras-loves-torchtext",
    packages=find_packages()
)

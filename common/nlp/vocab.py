from itertools import chain


class Vocab(object):
    def __init__(self, special_tokens=['░']):
        self.itos = list(special_tokens) if special_tokens else []

    @classmethod
    def from_file(cls, filename):
        with open(filename, 'r', encoding='utf8') as fp:
            tokens = fp.read().split('\r')
            vocab = Vocab(special_tokens=[])
            vocab.fit(tokens)

    def _build_dictionary(self):
        self.stoi = {ch: i for i, ch in enumerate(self.itos)}

    def fit(self, data):
        tokens = set(chain.from_iterable(data))
        self.itos += list(tokens - set(self.itos))
        self._build_dictionary()

    def to_seq(self, text):
        if isinstance(text, list):
            return [self.to_seq(t) for t in text]
        return [self.stoi[token] for token in text]

    def to_text(self, seq):
        if len(seq) and isinstance(seq[0], list):
            return [self.to_text(s) for s in seq]
        return ''.join([self.itos[idx] for idx in seq])

    def __len__(self):
        return len(self.itos)

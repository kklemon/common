import re
import nltk
from unidecode import unidecode

re_multispace = re.compile(r'\s+')


def normalize(s,
              remove_regex=None,
              lowerize=True,
              sub_multispace=True,
              sub_unicode=True):
    if remove_regex:
        s = re.sub(remove_regex, '', s)
    if lowerize:
        s = s.lower()
    if sub_multispace:
        s = re_multispace.sub(s, ' ')
    if sub_unicode:
        s = unidecode(s)
    return s

def tokenize(s, word_split=True):
    if normalize:
        s = normalize(s)
    if word_split:
        return nltk.wordpunct_tokenize(s)
    return list(s)
